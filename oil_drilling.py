def in_a_group(pair):
    """
    find out if a row/col pair is in a group
    :param groups:
    :param pair:
    :return:
    """
    found = False
    i = 0
    while i < len(groups) and not found:
        if pair in groups[i]:
            found = True
        i += 1

    return found


def surrounding_oil(group, row, col):
    """
    look for surrounding oil wells and add them to the given group if they do not already belong to one
    :param group:
    :param row:
    :param col:
    :return:
    """
    if row > 0:
        # only check the row above if we are not in the first row
        if col > 0:
            # only check the upper left if we are not in the first column
            if grid[row-1][col-1] == 1 and not in_a_group((row-1, col-1)):
                # upper left
                group.append((row - 1, col - 1))
                surrounding_oil(group, row - 1, col - 1)
        if grid[row-1][col] == 1 and not in_a_group((row-1, col)):
            # above
            group.append((row - 1, col))
            surrounding_oil(group, row - 1, col)
        # only check the upper right if we are not in the last column
        if col < len(grid[row]) - 1:
            if grid[row-1][col+1] == 1 and not in_a_group((row-1, col+1)):
                # upper right
                group.append((row-1, col+1))
                surrounding_oil(group, row-1, col+1)
    # only check to the left if we are not in the first column
    if col > 0:
        if grid[row][col-1] == 1 and not in_a_group((row, col-1)):
            # left
            group.append((row, col - 1))
            surrounding_oil(group, row, col - 1)
    # only check to the right if we are not in the last column
    if col < len(grid[row]) - 1:
        if grid[row][col+1] == 1 and not in_a_group((row, col+1)):
            # right
            group.append((row, col + 1))
            surrounding_oil(group, row, col + 1)
    # only check the row below if we are not in the last row
    if row < len(grid) - 1:
        # only check the lower left if we are not in the first column
        if col > 0:
            if grid[row+1][col-1] == 1 and not in_a_group((row+1, col-1)):
                # lower left
                group.append((row+1, col-1))
                surrounding_oil(group, row + 1, col - 1)
        if grid[row+1][col] == 1 and not in_a_group((row+1, col)):
            # below
            group.append((row + 1, col))
            surrounding_oil(group, row + 1, col)
        # only check the lower right if we are not in the last column
        if col < len(grid[row]) - 1:
            if grid[row+1][col+1] == 1 and not in_a_group((row+1, col+1)):
                # lower right
                group.append((row+1, col+1))
                surrounding_oil(group, row+1, col+1)


def find_groupings(grid):
    for row, rowval in enumerate(grid):
        for col, colval in enumerate(grid[row]):
            if colval == 1:
                if not in_a_group((row, col)):
                    # new root element
                    groups.append([(row, col)])
                    new_group = groups[len(groups)-1]

                    # find all surrounding oil wells 
                    surrounding_oil(new_group, row, col)

    return groups


groups = []
# the oil landscape "grid"
grid = [
    [1, 0, 0, 0, 1],
    [0, 1, 0, 1, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
]

groupings = find_groupings(grid)
num_drills = len(groupings)
print('%d drill spot(s) needed:' % num_drills)
for grouping in groupings:
    print(grouping)
